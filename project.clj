(defproject synq3/lein-apt "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[clojure-term-colors "0.1.0"]]
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}

  :profiles {:dev {:plugins [[cider/cider-nrepl "0.28.5"]]
                   :dependencies [[org.nrepl/incomplete "0.1.0"]]}}

  :eval-in-leiningen true)
