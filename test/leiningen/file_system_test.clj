(ns leiningen.file-system-test
  (:require [clojure.string :refer [join]]
            [clojure.test :refer :all]
            [leiningen.file-system :as fs])
  (:import java.lang.AssertionError))

(defn project-root
  "Returns the project root"
  []
  (.getCanonicalPath (clojure.java.io/file ".")))


(deftest filesystem-separator
  (testing "Returns the JVM System Default file separator"
    (let [separator (.getSeparator (java.nio.file.FileSystems/getDefault))]
      (is (= separator fs/separator)))))

(deftest apt-root-returns-correct-path
  (testing "adds the apt directory to the end of the target path"
    (is (= (->> (project-root) (assoc {} :target-path) fs/apt-root :apt-root)
           (format "%s%s%s" (project-root) fs/separator "apt"))))
  (testing "returns the correct file separator"
    (is (= (->> (project-root) (assoc {} :target-path) fs/apt-root :file-separator)
           fs/separator)))
  (testing "throws AssertionError if target-path is not a non-empty string"
    (is (thrown? AssertionError (fs/apt-root nil)))
    (is (thrown? AssertionError (fs/apt-root 1234)))
    (is (thrown? AssertionError (fs/apt-root [])))
    (is (thrown? AssertionError (fs/apt-root {})))
    (is (thrown? AssertionError (fs/apt-root "")))))

(deftest apt-root-exists?-tests
;  (testing "invokes passed fn-exists?"

  (testing "throws AssertionError if target-path is not a non-empty string"
    (is (thrown? AssertionError (fs/apt-root-exists? nil)))
    (is (thrown? AssertionError (fs/apt-root-exists? 1234)))
    (is (thrown? AssertionError (fs/apt-root-exists? [])))
    (is (thrown? AssertionError (fs/apt-root-exists? {})))
    (is (thrown? AssertionError (fs/apt-root-exists? "")))))
