(ns leiningen.apt-check-test
  (:require [clojure.test :refer :all]
            [leiningen.apt-check :as ac])
  (:import java.lang.AssertionError))

(deftest debian?-tests 
  (testing "returns true if distribution is Debian"
    (is (true? (ac/debian? "Distributor ID: Debian")))
    (is (true? (ac/debian? "Distributor ID:      Debian           ")))
    (is (true? (ac/debian? "Distributor ID: DeBiaN")))
    (is (true? (ac/debian? "Distributor ID: debian"))))
  (testing "returns false if distribution is NOT Debian"
    (is (false? (ac/debian? "Distributor ID: RedHat")))
    (is (false? (ac/debian? "Distributor ID: Ubuntu"))))
  (testing "returns false if distribution ID is in an invalid format"
    (is (false? (ac/debian? "debian"))))
  (testing "throws AssertionError if distribution-id is not a string"
    (is (thrown? AssertionError (ac/debian? nil)))
    (is (thrown? AssertionError (ac/debian? [])))
    (is (thrown? AssertionError (ac/debian? {})))
    (is (thrown? AssertionError (ac/debian? false)))
    (is (thrown? AssertionError (ac/debian? true)))
    (is (thrown? AssertionError (ac/debian? 1234567)))))
