(ns leiningen.internal.check
  "This is a top level help."
  (:require [clojure.java.shell :refer [sh]]
            [clojure.string :refer [split trim upper-case]]
            [clojure.term.colors :refer :all]
            [leiningen.internal.util :refer [ERR-exit]]))

(defn debian?
  "Takes `distribution-id`, the output from running the command `lsb_release -i`,
   returns true if Debian, false if not."
  [distribution-id]
  (assert (string? distribution-id))
  (let [xs (split distribution-id #"\:")]
    (if (not (= (count xs) 2))
      false
      (-> (last xs) 
          trim
          upper-case
          (= "DEBIAN")))))

(defn apt-check 
  "Run lein-apt configuration checks."
  ;[project & args]
  [project]
  (try
    (print "checking for Debian Linux distribution ... ")
    (let [{:keys [exit out]} (sh "lsb_release" "-i")]  
      (when-not (= 0 exit)
        (ERR-exit "lsb_release command not found. Are you sure you're on a Linux machine?"))
      (if (true? (debian? out))
        (println "✅")
        (doall
          (println "❌")
          (ERR-exit (format "Debian distribution required but was not found")))))
    (catch java.io.IOException _ 
      (doall
        (println "❌")
        (flush)
        (ERR-exit "lsb_release command not found. Are you sure you're on a Linux machine?"))))

  (try
    (print "checking to ensure `dpkg-dev` package is installed ... ")
    (let [{:keys [exit out]} (sh "dpkg-query" "-W" "dpkg-dev")]  
      (if (= 0 exit)
        (println "✅")
        (doall
          (println "❌")
          (ERR-exit "package `dpkg-dev` was not found. Try running `sudo apt-get install dpkg-dev`"))))
    (catch java.io.IOException _ 
      (doall
        (println "❌")
        (flush)
        (ERR-exit "command `dpkg-query` is required but was not found. Verify you are on Debian and that the tool is installed."))))

  (println "✅" (green "build machine is configured correctly")))
