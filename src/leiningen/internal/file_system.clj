(ns leiningen.internal.file-system
  (:require [clojure.string :refer [blank?]]))

(def separator (.getSeparator (java.nio.file.FileSystems/getDefault)))

(defn apt-root
  "Takes `project`, returns the root folder for the APT package."
  [{:keys [target-path]}]
  (assert (not (blank? target-path)))
  {:apt-root       (format "%s%s%s" target-path separator "apt")
   :file-separator separator})

(defn debpackage-root
  "Takes `project` `deb-package`, returns the deb package root folder."
  [project deb-package]
  (let [deb-dir-name (format "%s_%s-%s_%s" 
                             (:package deb-package) 
                             (:version deb-package) 
                             (:release-number deb-package) 
                             (:architecture deb-package))
        apt-root-dir (:apt-root (apt-root project))]
    (format "%s%s%s" apt-root-dir separator deb-dir-name)))

(defn debian-folder
  ""
  [project deb-package]
  (let [dp-root (debpackage-root project deb-package)]
    (format "%s%s%s" dp-root separator "DEBIAN")))

(defn string->path
  "Takes `string-path`, converts it to a java.nio.file.Path"
  [string-path]
  (-> (format "file://%s" string-path)
      (java.net.URI. )
      java.nio.file.Paths/get))

(defn dir-exists?
  ""
  [string-path]
  (let [uri  (java.net.URI. (format "file://%s" string-path))
        path (java.nio.file.Paths/get uri)
        link-options (->> (java.nio.file.LinkOption/valueOf "NOFOLLOW_LINKS")
                          (conj [])
                          into-array)]
    (java.nio.file.Files/isDirectory path link-options)))

(defn apt-root-exists?
  "Takes `project`, returns the root folder for the APT package."
  ([project] (apt-root-exists? project dir-exists?))
  ([{:keys [target-path] :as project} exists?-fn]
    (assert (not (blank? target-path)))
    (exists?-fn (:apt-root (apt-root project)))))

(def x "/mnt/disks/ssd-array/penland365/Code/unum/engineering/hookshot/target/default/apt")

(def folder-attributes
  (let [permissions (java.nio.file.attribute.PosixFilePermissions/fromString "rwxr-xr-x")
        attributes  (java.nio.file.attribute.PosixFilePermissions/asFileAttribute permissions)]
    (into-array [attributes])))


(defn create-directory
  ""
  [string-path]
  (let [path         (string->path string-path)
        created-path (java.nio.file.Files/createDirectories path folder-attributes)] 
    created-path))

(defn create-apt-root
  "Takes `project`, creates the apt root." 
  ([project] (create-apt-root project create-directory))
  ([{:keys [target-path] :as project} create-dir-fn]
   (assert (false? (blank? target-path)))
   (create-directory (:apt-root (apt-root project)))))

(def y {:target-path "/mnt/disks/ssd-array/penland365/Code/unum/engineering/hookshot/target/uberjar"})
(def z 
{:package-name "hookshot",
 :description "The service that handles all things webhook related",
 :maintainers "Jeffrey Davis <jdavis@synq3.com",
 :version "3ef537020af038dbefbc252d192d2c540235ca09",
 :url "https://github.com/NovoLabs/unum/tree/develop/hookshot",
 :release-number 1,
 :architecture "all"})

(defn deb-package-dir-exists?
  "<package-name>_<version>-<release-number>_<architecture>
  "
  [project deb-package]
  (-> (debpackage-root project deb-package)
      dir-exists?))
      
(defn create-deb-package-dir
  ""
  ([project deb-package] (create-deb-package-dir project deb-package create-directory))
  ([{:keys [target-path] :as project} deb-package create-dir-fn]
    (-> (debpackage-root project deb-package)
        create-directory)))

(defn debian-dir-exists?
  ""
  [project deb-package]
  (-> (debian-folder project deb-package)
      dir-exists?))

(defn create-debian-dir
  ""
  ([project deb-package] (create-debian-dir project deb-package create-directory))
  ([{:keys [target-path] :as project} deb-package create-dir-fn]
    (-> (debian-folder project deb-package)
        create-directory)))
