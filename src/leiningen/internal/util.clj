(ns leiningen.internal.util
  (:gen-class)
  (:require [clojure.term.colors :refer [on-red]]))

(defn ERR 
  "It's an error."
  [s]
  (.println ^java.io.PrintStream *err* (on-red "ERR: " s)))

(defn ERR-exit
  "It's an ERR-exit."
  [s]
  (ERR s)
  (System/exit 1))
