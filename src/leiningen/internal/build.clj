(ns leiningen.internal.build
  (:require [clojure.java.io :refer [make-parents]]
            [clojure.pprint :as pp]
            [leiningen.internal.file-system :as fs]))

(def tp {:description "The service that handles all things webhook related"    
         :name         "hookshot"
         :ref          "3ef537020af038dbefbc252d192d2c540235ca09"
         :url          "https://github.com/NovoLabs/unum/tree/develop/hookshot"
         :lein-apt     {:architecture :all
                        :maintainer {:name "Jeffrey Davis", :email "jdavis@synq3.com"}
                        :depends    ["openjdk-17-jre"]
                        :release-number 1}})

(defrecord DebianControl [package version maintainer description homepage release-number
                          architecture depends])

(defn project+apt-conf->debian-control
  "Takes `project`, `apt-conf` maps, builds a DebianControl."
  [project apt-conf]
  (println apt-conf)
  (DebianControl. (:name project)
                  (:ref project)
                  (:maintainer apt-conf)
                  (:description project)
                  (:url project)
                  (:release-number apt-conf)
                  (:architecture apt-conf)
                  (:depends "openjdk-17-jre")))


(defn spit-debian-control
  "Takes `control`, formats it and writes it under the DEBIAN folder.
  
   See https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-format
   for more information on control file syntax."
 [control control-folder] 
 (let [s (format "Package: %s\nVersion: %s\nMaintainer: %s\nArchitecture: %s\nHomepage: %s\nDescription: %s"
                 (:package control)
                 (:version control)
                 (:maintainer control)
                 (:architecture control)
                 (:homepage control)
                 (:description control)
                 
                 )]
   (spit (format "%s/control" control-folder) s)
   (println s)))


;(defn tt
;  []
;  (spit-debian-control (project+apt-conf->debian-control tp (leiningen.apt/project->apt tp))
;                       "/home/penland365/Code/unum/engineering/hookshot/target/uberjar/apt/hookshot_3ef537020af038dbefbc252d192d2c540235ca09-1_all/DEBIAN"))
;     

(defn apt-build
  "Run lein-apt configuration checks."
  [project apt-conf]
  (let [control (project+apt-conf->debian-control project apt-conf)]
    (when-not (fs/apt-root-exists? project)
      (fs/create-apt-root project))
    (when-not (fs/deb-package-dir-exists? project control)
      (fs/create-deb-package-dir project control))
    (when-not (fs/debian-dir-exists? project control)
      (fs/create-debian-dir project control))
    (println "end of build-run")))