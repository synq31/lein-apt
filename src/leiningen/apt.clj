(ns leiningen.apt
  (:require [clojure.term.colors :refer [on-red]]
            [leiningen.internal.build :refer [apt-build]]
            [leiningen.internal.check :refer [apt-check]]
            [leiningen.internal.util :refer [ERR-exit]]))

(def tp {:description "The service that handles all things webhook related"    
         :name         "hookshot"
         :ref          "3ef537020af038dbefbc252d192d2c540235ca09"
         :lein-apt     {:architecture :all
                        :maintainer {:name "Jeffrey Davis", :email "jdavis@synq3.com"}
                        :release-number 1
                        :depends ["openjdk-17-jre"]}})

(defn validate-project
  [project]
  (when-not (map? (:lein-apt project))
    (ERR-exit "map entry `:lein-apt` is required in project.clj but was not found."))
  project)

(defrecord Deb [package-name description])
(defrecord Maintainer [user-name user-email])
(defrecord Conf [release-number architecture maintainer])

(defn lein-apt->maintainer
  "Takes `lein-apt` map from project.clj, converts it into a maintainer entry."
  [{:keys [maintainer]}]
  (format "%s <%s>" (:name maintainer) (:email maintainer)))

(defn project->apt
  "Takes `project`, builds a `apt` record."
 [{:keys [lein-apt] :as project}] 
 (Conf. (:release-number lein-apt) 
            (name (:architecture lein-apt))
            (lein-apt->maintainer lein-apt)))

(defn check
  "Runs configurations checks to ensure you can build a Debian package."
  [project]
  (apt-check project))

(defn build
  "Builds a Debian package from an uberjar and package layout.
   
This is more help."
  [project]
  (apt-build project (project->apt project)))
 
(defn
  ^{:subtasks [#'check #'build]
    :help-arglists '[[check] [build]]
    :doc "Package up uberjar and deb package layout into a Debian package."}
  apt
  [project sub-name]
  (case sub-name
    "check" (check (validate-project project)) 
    "build" (build (validate-project project))
    #_{:clj-kondo/ignore [:unresolved-namespace]}
    (leiningen.core.main/warn (format "Unknown task %s.  Please run lein help apt for available subtasks." 
                                      (on-red sub-name)))))